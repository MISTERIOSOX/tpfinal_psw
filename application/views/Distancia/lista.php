<script type="text/javascript">
    function setEliminar(id) {
        $("#iddistancia").attr('value', id);  
             
    }
</script>


<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Distancias</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>distancias"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item active">Distancias</a></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>nuevo-distancias" class="btn btn-sm btn-neutral">Nuevo</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Lista</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table id="distanciaTable" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort">N°</th>
                    <th scope="col" class="sort">Origen</th>
                    <th scope="col" class="sort">Destino</th>
                    <th scope="col" class="sort">Kilometros</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">

                  <?php if(!empty($data)):?>
                    <?php foreach($data as $value):?>
                    <tr>
                      
                      <th><?php echo $value->id_distancia; ?></th>
                      <td><?php echo $value->origen; ?></td>  
                      <td><?php echo $value->destino; ?></td> 
                      <td><?php echo $value->km; ?></td>                    
                      
                      
                      
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="<?php echo base_url()."distancias/".$value->id_distancia; ?>">Editar</a>
                            <a class="dropdown-item" href="javascript:void()" onclick="setEliminar('<?= $value->id_distancia ?>')" data-toggle="modal" data-target="#myModalEliminar">Eliminar</a>
                           
                          </div>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                  
                </tbody>
              </table>
            </div>
           

          </div>
        </div>
      </div>

      <div class="modal" id="myModalEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Cabezera -->
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Empleado</h4>                
            </div>
            <!-- Modal Cuerpo -->

            <div class="modal-body">
                <input type="hidden" value="" name="iddistancia" id="iddistancia"/> 
                Esta seguro de Eliminar la distancia <?php echo $value->origen?> a <?php echo $value->destino?>?
            </div>
            <!-- Modal Pie -->
            <div class="modal-footer">
                
                <a class="btn btn-primary" href="<?php echo base_url()."distancias/delete/".$value->id_distancia; ?>">Elimnar</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
          
        </div>
    </div>
</div>
    
      

