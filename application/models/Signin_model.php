<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin_model extends CI_Model {

	public function signIn($usuario, $password){
        $this->db->where("usuario", $usuario);
        $this->db->where("password", $password);
        $results = $this->db->get("usuarios");
        if ($results->num_rows() > 0) {
            return $results->row();
        }else{
            return false;
        }
    }

}
