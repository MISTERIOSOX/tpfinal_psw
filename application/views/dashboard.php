<!-- Header -->
<div class="header bg-primary pb-6">
<div class="container-fluid">
    <div class="header-body">
    <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
        <h6 class="h2 text-white d-inline-block mb-0">Inicio</h6>
        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Pantalla Principal</a></li>
            </ol>
        </nav>
        </div>
    </div>
    <!-- Card stats -->
    <div class="row">
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Usuarios</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_usuario; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-single-02"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Cliente</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_cliente; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-circle-08"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Localidades</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_localidad; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-square-pin"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Viaticos</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_viatico; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                    <i class="ni ni-collection"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Vehiculos</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_vehiculos; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-pink text-white rounded-circle shadow">
                    <i class="ni ni-bus-front-12"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Distancias</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_distancias; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-dark text-white rounded-circle shadow">
                    <i class="ni ni-compass-04"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Vales</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_vales; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-single-copy-04"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
        <div class="col-xl-6 col-md-6">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
            <div class="row">
                <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Ordenes</h5>
                <span class="h2 font-weight-bold mb-0"><?php  echo $cants->cant_ordenes; ?></span>
                </div>
                <div class="col-auto">
                <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">
                    <i class="ni ni-bag-17"></i>
                </div>
                </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
                <span class="text-nowrap">Total</span>
            </p>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
</div>