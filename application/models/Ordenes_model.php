<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenes_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE ordenes');
		return $this->db->insert("ordenes",$data);
	}

	public function update($data,$id){
		$this->db->where("id_viaje",$id);
		return $this->db->update("ordenes",$data);
	}

	public function getOrden($id){
		$this->db->select("*");
		$this->db->from("ordenes");
		$this->db->where("id_viaje",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getOrdenes(){
		$this->db->select("*");
		$this->db->from("ordenes");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_viaje", $id);
		$this->db->db_debug = false;
		if($this->db->delete("ordenes")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}
	public function getOrigen(){
		$this->db->select("*");
		$this->db->from("distancias");
		$results = $this->db->get();
		return $results->result(); 
	}
	public function getCliente(){
		$this->db->select("*");
		$this->db->from("clientes");
		$results = $this->db->get();
		return $results->result(); 
	}
	public function getVehiculo(){
		$this->db->select("*");
		$this->db->from("vehiculos");
		$results = $this->db->get();
		return $results->result(); 
	}
	public function getConductor(){
		$this->db->select("*");
		$this->db->from("usuarios");
		$results = $this->db->get();
		return $results->result(); 
	}
	

}