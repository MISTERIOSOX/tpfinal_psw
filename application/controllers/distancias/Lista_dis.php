<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista_dis extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Distancia_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
        }
        
    }

	public function index()
	{       
        $data = array("data" => $this->Distancia_model->getDistancias()); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('Distancia/lista',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/distancia');
       
    }
    public function borrar(){
        
    }
    public function delete($id){
      
        $resp=$this->Distancia_model->delete($id);
        $this->session->set_flashdata($resp[0],$resp[1]);
        redirect(base_url()."distancias");
        
       
                    
    }


}
