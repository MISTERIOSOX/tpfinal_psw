<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0"><?php echo $nombre ?></h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>usuario">Usuario</a></li>
                <li class="breadcrumb-item active"><?php echo $nombre ?></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>usuario" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Actualice los datos</h3>
                        <?php print_r(form_error("nombre")); ?>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="<?php echo base_url();?>usuarios/update/<?php echo $id; ?>" method="POST" enctype="multipart/form-data">
                    

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Apellido</label>
                                <input type="text" name="apellido" class="form-control <?php echo form_error('apellido') ? 'is-invalid':''?>" placeholder="ingrese apellido"  value="<?php echo form_error('apellido') ? set_value('apellido'): $apellido; ?>">
                                <div class="invalid-feedback"><?php echo form_error('apellido'); ?></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Nombre</label>
                                <input type="text" name="nombre" class="form-control <?php echo form_error('nombre') ? 'is-invalid':''?>" placeholder="ingrese nombre"  value="<?php echo form_error('nombre') ? set_value('nombre'): $nombre; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Usuario</label>
                                            <input type="text" name="usuario" class="form-control <?php echo form_error('usuario') ? 'is-invalid':''?>" placeholder="Nombre de Usuario"  value="<?php echo form_error('usuario') ? set_value('usuario') : $usuario; ?>">
                                            <div class="invalid-feedback"><?php echo form_error('usuario'); ?></div>
                                        </div>
                                        </div>
                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-labe">password</label>
                                            <input type="password" name="password" class="form-control <?php echo form_error('password') ? 'is-invalid':''?>" placeholder="Ingrese password"  value="<?php echo form_error('password') ? set_value('password') : $password; ?>">
                                            <div class="invalid-feedback"><?php echo form_error('password'); ?></div>
                                        </div>
                                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Rol</label>
                                <input type="text" name="rol" class="form-control <?php echo form_error('rol') ? 'is-invalid':''?>" placeholder="rol"  value="<?php echo form_error('rol') ? set_value('rol') : $rol; ?>">
                                <div class="invalid-feedback"><?php echo form_error('rol'); ?></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Telefono</label>
                                <input type="number" name="telefono" class="form-control <?php echo form_error('telefono') ? 'is-invalid':''?>" placeholder="ingrese telefono"  value="<?php echo form_error('telefono') ? set_value('telefono') : $telefono; ?>">
                                <div class="invalid-feedback"><?php echo form_error('telefono'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                                <label class="form-control-label">Direccion</label>
                                <input type="text" name="direccion" class="form-control <?php echo form_error('direccion') ? 'is-invalid':''?>" placeholder="ingrese Direccion"  value="<?php echo form_error('direccion') ? set_value('direccion') : $direccion; ?>">
                                <div class="invalid-feedback"><?php echo form_error('direccion'); ?></div>
                            </div>
                    
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success mt-4">Guardar</button>
                    </div>  
                
                </form>
            </div>
            </div>
        </div>
        </div>  

        