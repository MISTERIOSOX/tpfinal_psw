<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class chat extends CI_Controller {

    public function __construct(){
        parent::__construct();
       $this->load->model("Mensajes_model");
       $this->load->model("Log_model");
       $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
        }
        
    }

	public function index()
	{       
        $data = array("data" => $this->Mensajes_model->getCorreos()); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('correo/lista',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/chat');
        
    }
    
    public function redactar(){
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('correo/redactar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/chat');
    }
    public function getDataUsuario(){
        $resp = $this->Mensajes_model->getUsuarios();
        if($resp){
            echo json_encode($resp);
        }
    }
    public function save(){

        $de = $this->session->userdata("usuario");
        $para = $this->input->post("usuario");
        $asunto = $this->input->post("asunto");
        $mensaje = $this->input->post("mensaje");
        $usuarioactual=$this->session->userdata("nombre");

       
        $this->form_validation->set_rules("asunto","asunto","required");
        $this->form_validation->set_rules("mensaje","mensaje","required");
       
        

		if ($this->form_validation->run()==TRUE) {
    
			$data  = array(
                'de' => $de,
                'para' => $para,
                'asunto' => $asunto,
                'mensaje' =>$mensaje,
                'fecha_envio'=>date("y-m-d"),
                'hora_envio'=>date("H:i:s"),
                'leido'=>"no",
                
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Enviar mensaje',
				'descripcion'=>'el usuario '.$usuarioactual.' envio un nuevo mensaje a: '.$para.'',

			);
			$this->Log_model->save($data2);

            $this->Mensajes_model->save($data);
            
			$this->session->set_flashdata("success","Se envio mensaje correctamente!");
			redirect(base_url()."chat");
		}else{
            $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav',$data2);
            $this->load->view('correo/redactar');
            $this->load->view('layout/footer');
            $this->load->view('layout/js/user');
		}
    }
    public function delete($id){
            
        $resp=$this->Mensajes_model->delete($id);
        $this->session->set_flashdata($resp[0],$resp[1]);
        redirect(base_url()."chat");
                    
    }
   public function leer($id){
    $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
    $data = $this->Mensajes_model->getCorreo($id); 
    if($data){

    $this->load->view('layout/head');
    $this->load->view('layout/sidenav');
    $this->load->view('layout/topnav',$data2);
    $this->load->view('correo/detalle',$data);
    $this->load->view('layout/footer');
    $this->load->view('layout/js/distancia');

    }
    $data  = array(
        'leido' => "si",
        'fecha_lectura'=>date("y-m-d"),
        'hora_lectura'=>date("h:i:s"),
        
    );
    $this->Mensajes_model->update($data,$id);
   }


}
